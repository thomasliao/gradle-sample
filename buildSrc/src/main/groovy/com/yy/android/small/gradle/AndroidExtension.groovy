package com.yy.android.small.gradle

import org.gradle.api.Project
import com.android.build.gradle.tasks.ProcessAndroidResources
import org.gradle.api.Task


public class AndroidExtension extends BaseExtension {

    /**
     * File of release variant output
     */
    public class AndroidBuildTypeConfig {
        AndroidBuildTypeConfig(String _name) {
            name = _name
        }

        protected File pluginOutputFile

        protected String apkName

        /** Task of android packager */
        ProcessAndroidResources aapt

        /** Task of R.class jar */
        Task jar

        /** Tasks of small aar exploder */
        String name

    }

    /** Tasks of aar exploder */
    AndroidBuildTypeConfig debug
    AndroidBuildTypeConfig release
    String pluginId
    Integer loadMode
    String launchMode
    String loadPriority
    String dependencies

    AndroidBuildTypeConfig variant(String variantName) {
        if (variantName.equals("Debug")) {

        }
        if (variantName.equals("Release")) {

        }
        return null
    }

    protected void createConfig() {
        debug = new AndroidBuildTypeConfig("Debug")
        release = new AndroidBuildTypeConfig("Release")
    }

    AndroidExtension(Project project) {
        super(project)
        createConfig()
        loadMode = 0
        launchMode = ""
        loadPriority = "600000"
        dependencies = "testing"
    }
}