package com.yy.android.small.gradle

import com.android.build.gradle.api.BaseVariant
import com.android.build.gradle.internal.dsl.BuildType
import org.gradle.api.Project

/**
 * Gradle plugin class to package 'application' or 'library' project as a .so plugin.
 */
abstract class BundlePlugin extends AndroidPlugin {

    @Override
    void apply(Project project) {
        super.apply(project)
    }

    /**
     * small extension
     * @return
     */
    @Override
    protected Class<? extends BaseExtension> getExtensionClass() {
        return BundleExtension.class
    }

    /**
     * get small extension
     * @return
     */
    @Override
    protected BundleExtension getSmall() {
        return project.small
    }

    @Override
    protected void afterEvaluate() {
        super.afterEvaluate()
        BuildType buildType = android.buildTypes.find { it.name = 'release' }

        Project hostProject = rootSmall.hostProject

        if (hostProject != null) {
            com.android.build.gradle.BaseExtension hostAndroid = hostProject.android
            def hostDebugBuildType = hostAndroid.buildTypes.find { it.name = 'debug' }
            def hostReleaseBuildType = hostAndroid.buildTypes.find { it.name = 'release' }

            // Copy host signing configs
            def sc = hostReleaseBuildType.signingConfig ?: hostDebugBuildType.signingConfig
            buildType.setSigningConfig(sc)
        }
    }

    @Override
    protected void configureVariant(BaseVariant variant) {
        super.configureVariant(variant)

        // Set output file (*.so)



    }

    protected def getOutputFile(BaseVariant variant) {
        def appId = variant.applicationId
        if (appId == null) return null

        BundleExtension.BundleBuildTypeConfig config = small.variant(variant.name.capitalize())
        BuildType buildType = androidBuildTypes.find { it.name == variant.name }




    }







}