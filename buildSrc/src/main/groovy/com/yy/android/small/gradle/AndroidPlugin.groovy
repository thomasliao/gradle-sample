package com.yy.android.small.gradle

import com.android.build.gradle.internal.api.ApplicationVariantImpl
import org.gradle.api.Project
import org.gradle.api.artifacts.ArtifactCollection
import com.android.build.gradle.internal.pipeline.TransformTask
import com.android.build.gradle.internal.transforms.ProGuardTransform
import com.android.build.gradle.api.BaseVariant

class AndroidPlugin extends BasePlugin {

    void apply(Project project) {
        super.apply(project)
    }

    @Override
    protected Class<? extends BaseExtension> getExtensionClass() {
        return AndroidExtension.class
    }

    @Override
    protected AndroidExtension getSmall() {
        return (AndroidExtension) project.small
    }

    protected RootExtension getRootSmall() {
        return project.rootProject.small
    }

    protected com.android.build.gradle.BaseExtension getAndroid() {
        return project.android
    }

    @Override
    protected void configureProject() {
        super.configureProject()

        project.beforeEvaluate {
            beforeEvaluate()
        }

        if (!shouldParsePluginScript) return

        project.afterEvaluate {
            afterEvaluate()

            if (!android.hasProperty("applicationVariants")) return

            android.applicationVariants.all { ApplicationVariantImpl variant ->
                ArtifactCollection jars = variant.variantData.scope.getArtifactCollection(ConsumedConfigType.RUNTIME_CLASSPATH, ArtifactScope.ALL, ArtifactType.EXPLODED_AAR)
                jars.artifacts.each {

                }

                // Configure ProGuard if needed
                if (variant.buildType.minifyEnabled) {
                    def variantName = variant.name.capitalize()
                    def proguardTaskName = "transformClassesAndResourcesWithProguardFor$variantName"
                    def proguard = (TransformTask) project.task[proguardTaskName]
                    def pt = (ProGuardTransform) proguard.getTransform()
                    configureProguard(variant, proguard, pt)
                }

                configureVariant(variant)
            }
        }
    }

    protected void beforeEvaluate() {}

    protected void afterEvaluate() {
        def preBuild = project.tasks['preBuild']

        preBuild.doFirst {
            hookPreBuild()
        }
    }

    protected void configureProguard(BaseVariant variant, TransformTask proguard, ProGuardTransform pt) {
        //keep support library
        pt.dontwarn('android.support.**')
        pt.keep('class android.support.** { *; }')
        pt.keep('interface android.support.** { *; }')

        //keep small library
        pt.dontwarn('com.yy.android.small.**')
        pt.keep('class com.yy.android.small.Small { public *; }')
        pt.keep( 'class com.yy.android.small.Bundle { public *; }')
        pt.keep('interface com.yy.android.small.** { *; }')

        //keep classes and interfaces with @keep annotation
        pt.keep('class android.support.annotation.**')
        pt.keep('@android.support.annotation.Keep class * { public *; }')
        pt.keep('@android.support.annotation.Keep interface * { *; }')

    }

    protected void hookPreBuild() { }

    protected void configureVariant(BaseVariant variant) {
        //Init default output file (*apk)
        //Hook variant tasks
        variant.assemble.doLast {
            tidyUp()
        }
    }

}