package com.yy.android.small.gradle

public enum PluginType {
    Unknown(0),
    Host(1),
    App(2),

    private int value

    public PluginType(int type) {
        this.value = value
    }
}