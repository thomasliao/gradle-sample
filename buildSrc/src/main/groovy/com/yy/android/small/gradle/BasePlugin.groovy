package com.yy.android.small.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import org.slf4j.Logger

/*  contains:

- 是否执行插件脚本
- Log

 */

public abstract class BasePlugin implements Plugin<Project> {

    protected boolean shouldParsePluginScript

    protected Project project

    @Override
    void apply(Project project) {
        this.project = project

        createExtension()

        if (Log.out == null) {
            Log.out = project.getLogger()
        }

        def sp = project.gradle.startParameter
        def p = sp.projectDir
        def t = sp.taskNames[0]
        if (p == null || p == project.rootProject.projectDir) {
            //gradlew buildLib | buildBundle
            if (t == 'buildBundleDebug' || t == 'buildBundleRelease') shouldParsePluginScript = true
        } else if (t == 'assembleRelease' || t == 'aR') {
            // gradlew -p [project.name] assembleRelease
            shouldParsePluginScript = true
        }
        def re = sp.taskNames.find { it == 'build' }
        if (re != null) {
            //gradlew xxx build
            shouldParsePluginScript = true
        }
    }

    protected void createExtension() {
        //add the 'small' extension object
        project.extensions.create('small', getExtensionClass(), project)
    }

    protected void configureProject() {

    }

    protected void createTask() {

    }

    protected <T extends BaseExtension> T getSmall() {
        return (T) project.small
    }

    protected PluginType getPluginType() { return PluginType.Unknown }

    protected abstract Class<? extends BaseExtension> getExtensionClass()

    /** Restore state for DEBUG mode */
    protected void tidyUp() {}

    final class Log {
        protected static Logger out

        static void header(String text) {
            out.print('[Small]')
            out.println(text)
        }

        static void success(String text) {
            out.printf('\t%-64s', text)
            out.print('[ OK ]')
            out.println()
        }

        static void warn(String text) {
            out.println('\t%s', text)
        }

        static void footer(String text) {
            out.printf('\t%s\n', text)
        }
    }

}