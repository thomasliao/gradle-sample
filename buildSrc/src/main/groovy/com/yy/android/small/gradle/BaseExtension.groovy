package com.yy.android.small.gradle

import org.gradle.api.Project


public class BaseExtension {

    public static final String FD_INTERMEDIATES = "intermediates"

    /** Package id of bundle */
    int packageId = 0
    String packageIdStr = '0'

    /** Bundle type */
    PluginType type

    /** Index of building loop */
    int buildIndex

    public BaseExtension(Project project) {

    }

}