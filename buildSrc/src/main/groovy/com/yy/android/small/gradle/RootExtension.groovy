package com.yy.android.small.gradle

import org.gradle.api.Project
import org.gradle.util.VersionNumber

public class RootExtension extends BaseExtension {

    private static final String FD_BUILD_SMALL = 'build/small'
    private static final String FD_OUTPUT_DIR = 'target'
    public static final String EXPORT_CLASS_FILE_NAME = 'export-class.txt'
    public static final String HIDE_CLASS_FILE_NAME = 'hide-class.txt'
    public static final String MAPPING_FILTER_FILE_NAME = 'mapping_filter.txt'

    /** The minimum small aar version required */
    private static final String REQUIRED_AAR_VERSION = '0.0.1'
    private static final VersionNumber REQUIRED_AAR_REVISION = VersionNumber.parse(REQUIRED_AAR_VERSION)

    /**
     * Version of aar com.yy.android.small:small
     * default to `gradle-small' plugin version
     */
    String aarVersion

    /**
     * Host module name
     * default to `app'
     */
    String hostModuleName

    /** The parsed revision of `aarVersion' */
    private VersionNumber aarRevision

    /**
     * Strict mode, <tt>true</tt> if keep only resources in bundle's res directory.
     */
    private strictSplitResources = true

    /** Count of libraries */
    protected int libCount

    /** Count of bundles */
    protected int bundleCount

    /** Project of Small AAR module */
    protected Project smallProject

    /** Project of host */
    protected Project hostProject

    /** Project of bundle */
    protected Set<Project> bundleProjects

    /** Directory to output bundles (*.so) */
    protected File outputBundleDir

    private File preBuildDir

    protected String mP // the executing gradle project name

    protected String mT // the executing gradle task name

    public Set<String> commonDepend = new HashSet<String>()

    RootExtension(Project project) {
        super(project)

        preBuildDir = new File(project.projectDir, FD_BUILD_SMALL)
        outputBundleDir = new File(project.projectDir, FD_OUTPUT_DIR)

        //Parse gradle task
        def sp = project.gradle.startParameter
        def t = sp.taskNames[0] //task name
        if ( t != null) {
            def p = sp.projectDir
            def pn = null //project name
            if ( p == null) {
                if (t.startsWith(':')) {
                    // gradlew :app.main:assembleRelease
                    def tArr = t.split(':')
                    if (tArr.length == 3) {
                        pn = tArr[1]
                        t = tArr[2]
                    }
                }
            } else if (p != project.rootProject.projectDir){
                // gradlew -p [project.name] assembleRelease
                pn = p.name
            }
            mP = pn
            mT = t
        }
    }







}