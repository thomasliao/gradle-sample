package com.yy.android.small.gradle

import org.gradle.api.Project


public class BundleExtension extends AndroidExtension {

    protected File buildDir
    protected File intermediatesDir
    protected File pluginOutputDir
    protected File pluginInfoOutputDir
    /** Directory of pre-build R.txt */
    protected File referenceResourceDir
    /** Directory of prepared dependencies */
    protected File referenceLibsDir

    public class BundleBuildTypeConfig extends AndroidExtension.AndroidBuildTypeConfig {
        BundleBuildTypeConfig(String _name) {
            super(_name)
        }

        File pluginOutputDir
        File pluginInfoOutputDir
    }

    @Override
    protected void createConfig() {
        debug = new BundleBuildTypeConfig("Debug")
        release = new BundleBuildTypeConfig("Release")
    }

    BundleExtension(Project project) {
        super(project)
        buildDir = new File(project.buildDir, "small")
        intermediatesDir = new File(buildDir, FD_INTERMEDIATES)
        referenceResourceDir = new File(intermediatesDir, "referenceResource")
        referenceLibsDir = new File(intermediatesDir, "referenceLibs")
        pluginOutputDir = new File(buildDir, "output/plugin")
        pluginInfoOutputDir = new File(buildDir, "output/pluginInfo")
    }
}