package com.yy.android.small.gradle

import org.gradle.api.Project

class RootPlugin extends BasePlugin {

    private int buildLibIndex = 0
    private Map<String, Set<String>> bundleModules = [:]

    @Override
    void apply(Project project) {
        super.apply(project)
    }

    @Override
    protected Class<? extends BaseExtension> getExtensionClass() {
        return RootExtension.class
    }

    @Override
    protected RootExtension getSmall() {
        return (RootExtension) project.small
    }

    @Override
    protected void configureProject() {
        super.configureProject()

        def rootExt = small

        rootExt.bundleProjects = new HashSet<>()

        project.afterEvaluate {
            project.subprojects {
                it.apply plugin: AppPlugin
            }
        }

    }
}